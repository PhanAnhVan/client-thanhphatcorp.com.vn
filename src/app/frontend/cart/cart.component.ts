import { Component, OnInit, TemplateRef, ViewContainerRef, OnDestroy, SimpleChanges } from '@angular/core'
import { TableService } from '../../services/integrated/table.service'
import { CartService } from '../../services/apicart/cart.service'
import { Globals } from '../../globals'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { ToastrService } from 'ngx-toastr'

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit, OnDestroy {
    public width: number = document.body.getBoundingClientRect().width

    private id: number

    public hiden: boolean

    public amount: number

    public item: any

    public datacart = []

    public lengthdatacart: number

    public data: any

    public alert = { skip: false, message: '' }

    public cartComplete: any = { skip: false, code: '' }

    fm: FormGroup

    public cwstable = new TableService()

    public flags = true

    public show: any

    public connect

    public token: any = {
        addcart: 'api/addCart',
        menu: 'api/getmenu'
    }

    constructor(
        public router: Router,
        public cart: CartService,
        public fb: FormBuilder,
        public routerAct: ActivatedRoute,
        public globals: Globals,
        private toastr: ToastrService
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id']
            this.id == 1 ? (this.show = true) : (this.show = false)
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case 'addcart':
                    window.scroll(0, 0)
                    let type = res['status'] == 1 ? 'success' : res['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { timeOut: 1000 })

                    if (res['status'] == 1) {
                        this.cart.clear()
                        this.cartComplete.code = res.data.code
                        this.cartComplete.skip = true
                    }
                    break
                // case 'login':
                //     // case 'loginfacegoogle':
                //     if (res['status'] == 1) {
                //         setTimeout(() => {
                //             this.item = this.globals.CUSTOMER.get(true)
                //             console.log('🚀 ~ file: cart.component.ts ~ line 81 ~ CartComponent ~ setTimeout ~ this.item', this.item)
                //             this.fmConfigs(this.item)
                //         }, 200)
                //     }
                //     break

                case 'paymentCart':
                    // for (let i = 0; i < res.data.length; i++) {
                    //     if (res.data[i].link != 'bo-cong-thuong') {
                    //         this.payment.data.unshift(res.data[i]);
                    //     }
                    // }

                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.order.getlist()
        this.globals.send({ path: this.token.menu, token: 'paymentCart', params: { position: 'payment' } })

        this.item = this.globals.CUSTOMER.get(true)
        setTimeout(() => {
            this.fmConfigs(this.item)
        }, 300)
        this.hiden = true
    }

    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { name: item.name, phone: this.item.phone, address: this.item.address }
        this.fm = this.fb.group({
            name: [item.name],
            phone: [
                item.phone ? item.phone : '',
                [Validators.required, Validators.pattern(/^([_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5}))|(\d+$)$/)]
            ],
            address: [item.address ? item.address : '', [Validators.required]],
            note: ''
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    order = {
        data: [],

        getlist: () => {
            this.order.data = this.cart.get()
        },

        changeAmount: (amount, index) => {
            if (amount <= 0 || !Number.isInteger(amount)) {
                this.order.data[index].amount = 1
            }
        },

        removeItem: id => {
            this.cart.remove(id)
            this.order.getlist()
        },

        addCart: (amount, id) => {
            let data = this.cart.reduce()
            if (data[id]) {
                data[id].amount = +amount
            }
            this.cart.edit(data[id], id)
        },

        submit: () => {
            let data = { cart: this.order.getDataCart(), cartdetail: this.order.getItemDetail(), item: this.order.getItem() }

            if (this.flags) {
                this.flags = !this.flags
                data.cart['customer_id'] = this.globals.CUSTOMER.get(true).id

                this.globals.send({ path: this.token.addcart, token: 'addcart', data: data })
            }
        },

        getItem: () => {
            let res = {
                id: this.item.id ? this.item.id : 0,
                email: this.item.email ? this.item.email : '',
                name: this.item.name ? this.item.name : '',
                data: this.cart.get()
            }
            return res
        },

        getDataCart: () => {
            let cart = {
                code: new Date().valueOf(),
                day_start: new Date(),
                delivery_status: 1,
                customer_id: this.item.id,
                delivery_address: this.datacart['address'],
                price_total: this.cart.total(),
                phone: this.datacart['phone'],
                amount_total: this.cart.amount(),
                note: this.datacart['note'],
                status: 1
            }
            this.order.getItem()
            return cart
        },

        getItemDetail: () => {
            return Object.values(
                this.cart.get().reduce((n, o, i) => {
                    n[i] = {
                        cart_id: 0,

                        product_id: +o.id_products,

                        amount: o.amount,

                        price: +o.price,

                        images: o.images,

                        attribute: this.order.getAttribute(o.attribute),

                        total: (isNaN(+o.amount) ? 0 : +o.amount) * (isNaN(+o.price) ? 0 : +o.price),

                        status: 1,

                        note: o.note ? o.note : ''
                    }
                    return n
                }, {})
            )
        },

        dataCart: () => {
            this.datacart = this.fm.value
            if (!this.fm.valid) {
                this.alert.skip = true
                this.alert.message = 'Bạn chưa nhập đầy đủ thông tin giao hàng.Vui lòng kiểm tra lại.'
                this.lengthdatacart = Array(this.datacart).length
                window.scrollTo({
                    top: 0,
                    left: 0,
                    behavior: 'smooth'
                })
                return false
            } else {
                this.alert.skip = false
                this.alert.message = ''
                this.lengthdatacart = Array(this.datacart).length
                window.scrollTo({
                    top: 0,
                    left: 0,
                    behavior: 'smooth'
                })
                return true
            }
        },
        getAttribute: data => {
            for (let i = 0; i < data.length; i++) {
                delete data[i].attribute_id
                delete data[i].id
                delete data[i].group
                delete data[i].value
            }
            for (let i = 0; i < data.length; i++) {
                data[i].value = data[i].valueChoose
                delete data[i].valueChoose
            }
            data = JSON.stringify(data)
            return data
        }
    }

    payment = {
        data: [{ id: 0, images: '../../../assets/img/cash.png', name: 'FECart.payBase' }]
    }

    handleCustomerValue = event => this.fmConfigs(event)
}
