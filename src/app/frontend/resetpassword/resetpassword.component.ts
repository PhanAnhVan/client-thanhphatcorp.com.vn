import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Globals } from '../../globals'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ToastrService } from 'ngx-toastr'

@Component({
    selector: 'app-resetpassword',
    templateUrl: './resetpassword.component.html',
    styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
    public connect
    fm: FormGroup
    public value_token: any
    public token: any = {
        checkToken: 'api/checkTokenResetPassword',
        changePassword: 'api/changePassword'
    }
    public checkToken: boolean = true
    public flag = true
    constructor(
        public route: ActivatedRoute,
        public globals: Globals,
        public router: Router,
        public fb: FormBuilder,
        public toastr: ToastrService
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'changePassword':
                    let type = res['status'] == 1 ? 'success' : res['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](res['message'], type, { timeOut: 1000 })
                    if (res['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate(['/'])
                        }, 2000)
                    }
                    break

                case 'checkTokenResetPassword':
                    if (res.status == 1) {
                        this.fm = this.fb.group({
                            id: res.data['id'],
                            password: ['', [Validators.required]],
                            token: this.value_token
                        })
                    } else {
                        this.checkToken = false
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.value_token = params.token
        })
        this.globals.send({ path: this.token.checkToken, token: 'checkTokenResetPassword', params: { value_token: this.value_token } })
    }

    onSubmit() {
        if (this.flag) {
            this.flag = false
            let data = this.fm.value
            this.globals.send({ path: this.token.changePassword, token: 'changePassword', data: data })
        }
    }
}
