import { Component, Input, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'

@Component({
    selector: 'app-inforuser',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class InfoUserComponent implements OnInit, OnDestroy {
    @Input('id') id: number

    fm: FormGroup

    public connect: any
    public phone: number

    public token: any = {
        update: 'api/updateCustomer',
        getrow: 'api/getRowCustomer'
    }

    constructor(
        public fb: FormBuilder,
        public router: Router,
        public globals: Globals,
        public translate: TranslateService,
        private toastr: ToastrService
    ) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'getrow':
                    this.fmConfigs(response['data'])

                    break

                case 'updateCustomer':
                    let type = response['status'] == 1 ? 'success' : response['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](response['message'], type, { timeOut: 1000 })
                    if (response['status'] == 1) {
                        this.globals.CUSTOMER.set(response['data'], true)
                        setTimeout(() => {
                            this.router.navigate(['/thong-tin-khach-hang'])
                        }, 300)
                    }
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        let item = this.globals.CUSTOMER.get(true)

        this.fmConfigs(item)
    }

    // ngOnChanges(changes: SimpleChanges): void {
    //     console.log(this.id);
    //     if (this.id && this.id != 0) {

    //         this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
    //     }
    // }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    onSubmit() {
        let data = this.fm.value
        data.status = 1
        this.globals.send({ path: this.token.update, token: 'updateCustomer', data: data })
    }

    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { type: 1, sex: 1 }
        this.fm = this.fb.group({
            sex: +item.sex,
            email: [
                item.email ? item.email : '',
                [
                    Validators.required,
                    Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)
                ]
            ],
            phone: [item.phone ? item.phone : '', [Validators.pattern(/[0-9\+\-\ ]/)]],
            address: item.address ? item.address : '',
            name: [item.name ? item.name : '', [Validators.required]],
            id: +item.id ? +item.id : 0
        })
    }
}
