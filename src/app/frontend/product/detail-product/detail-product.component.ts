import { Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { SlidesOutputData } from 'ngx-owl-carousel-o'
import { ToastrService } from 'ngx-toastr'
import { Subscription } from 'rxjs'
import { Globals } from '../../../globals'
import { CartService } from '../../../services/apicart/cart.service'

@Component({
    selector: 'app-detail-product',
    templateUrl: './detail-product.component.html',
    styleUrls: ['./detail-product.component.scss'],
    providers: [CartService]
})
export class DetailProductComponent implements OnInit, OnDestroy {
    public connect: Subscription

    public listImages = { data: [], cached: [], active: 0 }
    public data: any = {}
    public width: number
    public widthOwl: number
    public link: any
    public activeSlides: SlidesOutputData
    public selected: any

    public token: any = {
        getProductDetail: 'api/getProductDetail'
    }

    constructor(
        public route: ActivatedRoute,
        public router: Router,
        public toastr: ToastrService,
        public globals: Globals,
        public apiCart: CartService
    ) {
        this.width = document.body.getBoundingClientRect().width
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getProductDetail':
                    this.data = res.data
                    let listimages = res.data.listimages && res.data.listimages.length > 5 ? JSON.parse(res.data.listimages) : []
                    this.listImages.cached = listimages
                    if (this.data.images && this.data.images.length > 4) {
                        this.listImages.cached = [this.data.images].concat(listimages)
                    } else {
                        this.data.images = listimages.length > 0 ? listimages[0] : ''
                    }
                    this.listImages.data = Object.values(this.listImages.cached)
                    this.related.data = res.data.related

                    res.data.attribute?.length && this.attribute._ini(res.data.attribute)

                    setTimeout(() => {
                        if (window['openfancybox']) {
                            window['openfancybox']()
                        }
                    }, 1000)

                    this.cart.data = {
                        id_products: this.data.id,

                        path: this.data.pages_link + '/' + this.data.link,

                        name: this.data.name,

                        attribute: [],

                        images: this.globals.BASE_API_URL + 'public/products/' + this.data.images,

                        price: this.data.price_sale && +this.data.price_sale != 0 ? this.data.price_sale : this.data.price,

                        amount: 1
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.link = params.link
            setTimeout(() => {
                this.globals.send({
                    path: this.token.getProductDetail,
                    token: 'getProductDetail',
                    params: { link: this.link }
                })
            }, 50)
        })

        setTimeout(() => {
            this.renderHtml()
        }, 1000)

        this.widthOwl = document.getElementById('owl-carousel').offsetWidth / (this.width > 415 ? 5 : 3) - (this.width > 415 ? 15 : 20)
    }

    isActive(images: any, index: number) {
        this.data.images = images
        this.listImages.active = index
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    renderHtml = () => {
        let main = document.getElementById('contentDetail')
        if (main) {
            let el = main.querySelectorAll('table')
            7
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement('div')
                    div.className = 'table-responsive table-bordered m-0 border-0'
                    el[i].parentNode.insertBefore(div, el[i])
                    el[i].className = 'table'
                    el[i].setAttribute('class', 'table')
                    let cls = el[i].getAttribute('class')
                    el[i]
                    let newhtml = "<table class='table'>" + el[i].innerHTML + '</table>'
                    el[i].remove()
                    div.innerHTML = newhtml
                }
            }
            let image = main.querySelectorAll('img')
            if (image) {
                for (let i = 0; i < image.length; i++) {
                    let a = document.createElement('div')
                    a.className = 'images-deatil d-inline'
                    image[i].parentNode.insertBefore(a, image[i])
                    let style = image[i].style.cssText
                    let src = image[i].currentSrc
                    let html =
                        `<a  class="fancybox" data-fancybox="images-preview" data-thumbs="{&quot;autoStart&quot;:true}" href="` +
                        src +
                        `">
                         <img  src="` +
                        src +
                        `" style="` +
                        style +
                        `" alt="` +
                        this.data.name +
                        `">
                    </a>`
                    image[i].remove()
                    a.innerHTML = html
                }
            }
        }
    }

    public library = {
        libraryOptions: {
            loop: false,
            autoplayTimeout: 8000,
            autoplaySpeed: 1500,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            dots: false,
            navSpeed: 500,
            dotsData: true,
            items: 1,
            navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
            nav: true
        },

        getPassedData: (data: SlidesOutputData) => {
            this.activeSlides = data
            this.selected = data.slides[0] ? data.slides[0].id : ''
            this.data.images = this.selected
            setTimeout(() => {
                this.library.onClickImageChild(this.selected + '-child')
            }, 200)
        },

        onClickImageChild: (id: string) => {
            let items: any = document.querySelectorAll('.img-child')
            for (let i = 0; i < items.length; i++) {
                items[i].style.opacity = 0.5
                items[i].classList.remove('border')
                items[i].classList.remove('border-success')
            }
            document.getElementById(id).style.opacity = '1'
            document.getElementById(id).classList.add('border')
            document.getElementById(id).classList.add('border-success')
        }
    }

    related = {
        data: [],
        relatedOptions: {
            autoWidth: true,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: true,
            margin: 10,
            responsive: {
                0: {
                    items: 2
                },
                700: {
                    items: 3
                },
                940: {
                    items: 5
                }
            },
            dots: false,
            nav: true,
            navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"]
        }
    }

    cart = {
        amount: 1,

        data: <any>{},

        changeAmount: () => {
            if (this.cart.amount <= 0 || !Number.isInteger(this.cart.amount)) {
                this.cart.amount = 1
            }
        },

        addCart: () => {
            this.cart.data.id = this.data.id

            this.cart.data.amount = this.cart.data.amount

            this.cart.data.attribute = this.attribute.dataChoose

            let data = this.apiCart.reduce()

            if (data[this.cart.data.id]) {
                let a = isNaN(+data[this.cart.data.id].amount) ? 1 : +data[this.cart.data.id].amount

                this.cart.data.amount = +a + this.cart.amount
            }

            this.apiCart.edit(this.cart.data, this.cart.data.id)

            // var element = document.getElementById('cart')

            window.scroll({ top: 0, behavior: 'smooth' })

            document.getElementById('notification').classList.add('d-block')
        }
    }

    attribute = {
        data: [],
        dataChoose: [],
        activeSize: '',
        activeColor: '',

        _ini: data => {
            for (let i = 0; i < data.length; i++) {
                data[i].value = JSON.parse(data[i].value)
                data[i].group = i
            }

            this.attribute.data = data

            this.attribute.getActive(data)
        },

        getActive: data => {
            for (let i = 0; i < data.length; i++) {
                if (data[i].group == 0) {
                    this.attribute.activeSize = data[i].value[0]
                    this.attribute.dataChoose.push(data[i])
                    data[i].valueChoose = this.attribute.activeSize
                } else {
                    this.attribute.activeColor = data[i].value[0]
                    data[i].valueChoose = this.attribute.activeColor
                    this.attribute.dataChoose.push(data[i])
                }
            }
        },

        choose: (item, value) => {
            item.group == 0 ? (this.attribute.activeSize = value) : (this.attribute.activeColor = value)

            item.valueChoose = value
            if (this.attribute.dataChoose.length > 0) {
                let skip = true

                for (let i = 0; i < this.attribute.dataChoose.length; i++) {
                    if (this.attribute.dataChoose[i].name == item.name) {
                        skip = false

                        this.attribute.dataChoose[i] = item
                        break
                    }
                }
                if (skip == true) {
                    this.attribute.dataChoose.push(item)
                }
            } else {
                this.attribute.dataChoose.push(item)
            }
        }
    }
}
