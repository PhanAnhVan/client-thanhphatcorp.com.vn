import { Component, OnInit, SimpleChanges } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ChangeContext, LabelType, Options } from 'ng5-slider'
import { PageChangedEvent } from 'ngx-bootstrap/pagination'
import { Globals } from '../../../globals'
import { TableService } from '../../../services/integrated/table.service'
import { ToslugService } from '../../../services/integrated/toslug.service'
@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss'],
    providers: [ToslugService]
})
export class ListProductComponent implements OnInit {
    public connect

    public cwstable = new TableService()

    public searchValue = ''

    public show: number = -1

    public data: any = {}

    public categories: any = []

    public cateListId = []

    public token = {
        getProduct: 'api/getProduct',
        productCategory: 'api/productGroup'
    }

    public width = document.body.getBoundingClientRect().width

    public collapsedMenu = []

    public openMobileFilter = false

    private cols = [
        { title: 'FEProduct.sortName', field: 'name', filter: true, active: true },
        // { title: 'FEProduct.sortPrice', field: 'price', filter: true, type: 'number' }
    ]

    minValue: number = 0
    maxValue: number = 100
    public price: any = { min: 0, max: 0 }

    options: Options = {
        floor: 0,
        ceil: 100,
        translate: (value: number, label: LabelType): string => {
            switch (label) {
                case LabelType.Low:
                    return '<b style="font-size:14px">Min: ' + this.setnumber(value) + ' đ </b>'
                case LabelType.High:
                    return '<b style="font-size:14px">Max: ' + this.setnumber(value) + ' đ </b>'
                default:
                    return this.setnumber(value)
            }
        }
    }

    constructor(public globals: Globals, public route: ActivatedRoute, public router: Router, public toSlug: ToslugService) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getProduct':
                    this.data = []
                    this.data = res.data
                    this.show = this.data.list && this.data.list.length > 0 ? 1 : 0
                    this.cwstable._concat(this.data.list, true)
                    this.extract()
                    break

                case 'productCategory':
                    this.categories = this.compaid(res.data)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.cwstable._ini({
            data: [],
            cols: this.cols,
            keyword: 'getProduct',
            count: this.Pagination.itemsPerPage,
            sorting: { field: 'name', sort: '', type: 'number' }
        })

        this.route.params.subscribe(params => {
            this.openMobileFilter = false
            if ((params.link && params.link.length > 0) || (params.links && params.links.length > 0)) {
                this.globals.send({
                    path: this.token.getProduct,
                    token: 'getProduct',
                    params: { link: params.links && params.links.length > 0 ? params.links : params.link }
                })
            } else {
                this.globals.send({ path: this.token.getProduct, token: 'getProduct' })
            }
        })

        this.globals.send({ path: this.token.productCategory, token: 'productCategory' })
    }

    ngOnChanges(e: SimpleChanges) {}

    public Pagination = {
        maxSize: 5,
        itemsPerPage: 20,
        change: (event: PageChangedEvent) => {
            const startItem = (event.page - 1) * event.itemsPerPage
            const endItem = event.page * event.itemsPerPage
            this.cwstable.data = this.cwstable.cached.slice(startItem, endItem)
            var el = document.getElementById('ListData')
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            })
        }
    }

    extract = () => {
        let price = { min: 0, max: 0 }

        let group = {}
        if (this.cwstable.cachedList.length > 0) {
            price.min =
                +this.cwstable.cachedList[0].price_sale > 0 ? +this.cwstable.cachedList[0].price_sale : +this.cwstable.cachedList[0].price
        }
        this.cwstable.cachedList.reduce((n, o, i) => {
            if (o.page_id && +o.page_id > 0) {
                if (!group[o.page_id]) {
                    group[o.page_id] = { id: o.page_id, name: o.parent_name, count: 0 }
                }
                if (group[o.page_id]) {
                    group[o.page_id].count = +group[o.page_id].count + 1
                }
            }

            let p = +o.price_sale > 0 ? +o.price_sale : +o.price
            price.min = p < price.min ? p : price.min
            price.max = p > price.max ? p : price.max

            this.changeOptions({
                floor: 0,
                ceil: price.max
            })

            return n
        }, {})

        this.price = price
    }

    public filter(id) {
        if (id > 0) {
            this.cwstable._setFilter('page_id', id, 'in', 'number')
        } else {
            this.cwstable._delFilter('page_id')
        }
    }

    groupCategory = {
        data: [],
        value: [],

        filter: (id, skip = 0) => {
            let token = 'page_id'
            let filter = this.cwstable._getFilter(token)

            let data = !filter.value
                ? {}
                : filter.value.reduce((n, o) => {
                      n[o] = o
                      return n
                  }, {})

            if (data[id]) {
                this.groupCategory.value = filter.value.filter(item => {
                    return +item == +id ? false : true
                })
            } else {
                this.groupCategory.value.push(id)
            }
            if (this.groupCategory.value.length == 0) {
                this.cwstable._delFilter(token)
            } else {
                this.cwstable._setFilter(token, this.groupCategory.value, 'in')
            }
        }
    }

    setnumber(Val) {
        let a = new Number(Val)

        return a.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
    }

    public collapseMenu(id) {
        if (this.collapsedMenu.includes(id) === true) {
            var index = this.collapsedMenu.indexOf(id)
            this.collapsedMenu.splice(index, 1)
        } else {
            this.collapsedMenu.push(id)
        }
    }

    setMenuMobile(open) {
        // let list = document.getElementsByTagName('body')[0]
        // list.style.position = open ? 'fixed' : 'inherit'

        return (this.openMobileFilter = open)
    }

    compaid(data: any[]) {
        let list = []
        data = data.filter(function (item: { parent_id: string | number }) {
            let v = isNaN(+item.parent_id) && item.parent_id ? 0 : +item.parent_id
            v == 0 ? '' : list.push(item)
            return v == 0 || v == 3 ? true : false
        })

        let compaidmenu = (data: string | any[], skip: boolean, level = 0) => {
            level = level + 1

            if (skip == true) {
                return data
            } else {
                for (let i = 0; i < data.length; i++) {
                    let obj = []
                    list = list.filter(item => {
                        let skip = +item.parent_id == +data[i]['id'] ? false : true
                        if (skip == false) {
                            obj.push(item)
                        }
                        return skip
                    })
                    let skip = obj.length == 0 ? true : false
                    data[i]['level'] = level
                    data[i]['show'] = i == 0 ? true : false
                    data[i]['data'] = compaidmenu(obj, skip, level)
                }
                return data
            }
        }

        return compaidmenu(data, false)
    }

    onUserChangeEnd(changeContext: ChangeContext): void {
        this.cwstable._setFilter('price', [changeContext.value, changeContext.highValue], 'between')
    }

    changeOptions(option) {
        const newOptions: Options = Object.assign({}, this.options)

        newOptions.ceil = option.ceil

        newOptions.floor = option.floor

        this.options = newOptions

        this.minValue = option.floor

        this.maxValue = option.ceil
    }
}
