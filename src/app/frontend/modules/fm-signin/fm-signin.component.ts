import { Location } from '@angular/common'
import { Component, EventEmitter, OnDestroy, OnInit, Output, TemplateRef } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Router } from '@angular/router'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'

@Component({
    selector: 'app-fm-signin',
    templateUrl: './fm-signin.component.html',
    styleUrls: ['./fm-signin.component.css']
})
export class FmSigninComponent implements OnInit, OnDestroy {
    @Output('customerValue') customerValue = new EventEmitter<string>()

    fm: FormGroup
    fmresetPassword: FormGroup
    modalRef: BsModalRef

    public company: any
    public type = 'password'
    public flag = false
    public path = ''
    public checkLink = 0

    public width = document.body.getBoundingClientRect().width

    public token: any = {
        login: 'api/login',
        resetpassword: 'api/resetpassword'
    }

    private connect
    public data: any = {}

    constructor(
        public fb: FormBuilder,
        private modalService: BsModalService,
        private toastr: ToastrService,
        public router: Router,
        public globals: Globals,
        public location: Location
    ) {
        this.routerLogin()
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'loginCustomer':
                    this.flag = false
                    this.showNotification(res.status, res.message)

                    if (res.status !== 1) return
                    this.globals.CUSTOMER.set(res['data'], true)

                    // TODO: Transfer customer value to component cart if location is 'gio-hang'
                    const CART_ROUTER = '/gio-hang'
                    window.location.pathname === CART_ROUTER && this.customerValue.emit(res.data)

                    setTimeout(() => {
                        if (this.checkLink == 0) {
                            this.router.navigate(['/'])
                        } else if (this.checkLink == 1) {
                            this.router.navigate(['/gio-hang'])
                        }
                    }, 500)
                    break

                case 'resetpassword':
                    this.showNotification(res.status, res.message)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        if (this.globals.CUSTOMER.check(true)) {
            this.router.navigate(['/'])
        } else {
            let item = this.globals.CUSTOMER.get(true)
            this.fm = this.fb.group({
                email: [
                    item.email ? item.email : '',
                    [Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]
                ],
                password: [item.password ? item.password : '', [Validators.required]]
            })
        }
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe()
        }
    }

    showNotification = (status: number, message: string) => {
        const type = status === 1 ? 'success' : status === 0 ? 'warning' : 'danger'

        return this.toastr[type](message, type, { timeOut: 1000 })
    }

    onSubmit() {
        if (!this.flag && this.fm.valid) {
            this.flag = true
            this.globals.send({
                path: 'api/login',
                token: 'loginCustomer',
                data: this.fm.value
            })
        }
    }

    resertPass(template: TemplateRef<any>) {
        this.fmresetPassword = this.fb.group({
            email: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]]
        })
        this.modalRef = this.modalService.show(template, { class: 'modal-sm' })
    }

    confirm(): void {
        let data = this.fmresetPassword.value
        this.globals.send({ path: this.token.resetpassword, token: 'resetpassword', data: data })
        this.modalRef.hide()
    }

    decline(): void {
        this.modalRef.hide()
    }

    routerLogin() {
        this.path = this.location.path()
        switch (this.path) {
            case '/dang-nhap':
                this.checkLink = 0
                break

            case '/gio-hang':
                this.checkLink = 1
                break
            default:
                this.checkLink = -1
                break
        }
    }
}
