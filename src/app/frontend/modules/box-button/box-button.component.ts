import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-box-button',
    templateUrl: './box-button.component.html',
    styleUrls: ['./box-button.component.css']
})
export class BoxButtonComponent implements OnInit {
    @Input('name') name: any;
    @Input('class') class: any;
    constructor() { }



    ngOnInit() {
    }

}
