import { Component, Input, } from '@angular/core';

@Component({
  selector: 'box-content-grid',
  templateUrl: './box-content-grid.component.html',
  styleUrls: ['./box-content-grid.component.css'],

})
export class BoxContentGridComponent {
  @Input('item') item: any;
  @Input('images') images: boolean = true;
  @Input('border') border: boolean = true;
  constructor() { }
}
