import { Component, Input, OnInit } from '@angular/core'

@Component({
    selector: 'app-box-product',
    templateUrl: './box-product.component.html',
    styleUrls: ['./box-product.component.scss']
})
export class BoxProductComponent implements OnInit {
    @Input('item') item: any

    constructor() {}

    ngOnInit() {}
}
