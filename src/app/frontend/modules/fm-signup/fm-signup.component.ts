import { Component, EventEmitter, OnInit, Output, ViewContainerRef } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'
import { Location } from '@angular/common'

@Component({
    selector: 'app-fm-signup',
    templateUrl: './fm-signup.component.html',
    styleUrls: ['./fm-signup.component.css']
})
export class FmSignupComponent implements OnInit {
    @Output('customerValue') customerValue = new EventEmitter<string>()

    public checkLink = 0
    public path = ' '
    public type = 'password'
    public flag = false

    fm: FormGroup

    public token: any = {
        signup: 'api/signup'
    }
    public connect

    constructor(
        public fb: FormBuilder,
        public router: Router,
        public routerAtc: ActivatedRoute,
        public globals: Globals,
        private toastr: ToastrService,
        public location: Location
    ) {
        this.fmConfigs()
        this.routerRegister()
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'signup':
                    this.flag = false
                    let type = res['status'] == 1 ? 'success' : res['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](res['message'], type, { timeOut: 1000 })

                    if (res.status !== 1) return
                    this.globals.CUSTOMER.set(res['data'], true)

                    // TODO: Transfer customer value to component cart if location is 'gio-hang'
                    const CART_ROUTER = '/gio-hang'
                    window.location.pathname === CART_ROUTER && this.customerValue.emit(res.data)

                    // if (res['status'] == 1) {
                    if (this.checkLink == 0) {
                        setTimeout(() => {
                            this.router.navigate(['/'])
                        }, 500)
                    } else if (this.checkLink == 1) {
                        setTimeout(() => {
                            this.router.navigate(['/gio-hang'])
                        }, 500)
                    }
                    // }
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.routerAtc.params.subscribe((params: any) => {
            this.routerRegister()
        })
    }

    ngOnDestroy() {
        // if (this.connect) {
        this.connect.unsubscribe()
        // }
    }

    onSubmit() {
        if (!this.flag && this.fm.valid) {
            this.flag = true
            let data = this.fm.value
            this.globals.send({ path: this.token.signup, token: 'signup', data: data })
        }
    }

    routerRegister() {
        this.path = this.location.path()
        switch (this.path) {
            case '/dang-ky':
                this.checkLink = 0
                break

            case '/gio-hang':
                this.checkLink = 1
                break
            default:
                this.checkLink = -1
                break
        }
    }

    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1, sex: 1 }
        this.fm = this.fb.group({
            name: ['', [Validators.required, Validators.minLength(5)]],
            phone: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
            email: [
                '',
                [
                    Validators.required,
                    Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)
                ]
            ],
            password: ['', [Validators.required]],
            sex: +item.sex ? +item.sex : ''
        })
    }
}
