import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from "ngx-bootstrap/modal";
import { uploadFileService } from '../../services/integrated/upload.service';
import { ChangespasswordComponent } from './changespassword/changespassword.component';
import { GetlistComponent } from './getlist/getlist.component';
import { ProcessComponent } from './process/process.component';

const appRoutes: Routes = [
	{ path: '', redirectTo: 'get-list' },
	{ path: 'get-list', component: GetlistComponent },
	{ path: 'insert', component: ProcessComponent },
	{ path: 'update/:id', component: ProcessComponent },
	{ path: 'changespassword/:id', component: ChangespasswordComponent },
]

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild(appRoutes),
		ModalModule.forRoot(),
		AlertModule.forRoot(),
		TranslateModule,
		BsDatepickerModule.forRoot(),
	],
	providers: [uploadFileService],
	declarations: [
		GetlistComponent,
		ProcessComponent,
		ChangespasswordComponent,

	]
})
export class PersonalModule { }
