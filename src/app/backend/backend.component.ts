import { AfterViewInit, Component } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { Globals } from '../globals'

@Component({
    selector: 'app-backend',
    templateUrl: './backend.component.html'
})
export class BackendComponent implements AfterViewInit {
    public opened: boolean = true
    public height: number = window.innerHeight

    constructor(public globals: Globals, public translate: TranslateService) {
        this.translate.use('admin')
    }

    eventOpened() {
        this.opened = this.opened == true ? false : true
    }

    ngAfterViewInit() {}
}
