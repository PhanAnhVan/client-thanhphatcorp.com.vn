import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { AlertModule } from 'ngx-bootstrap/alert'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { TimepickerModule } from 'ngx-bootstrap/timepicker'
import { TagsService } from '../../services/integrated/tags.service'
import { EmailComponent } from './email/email.component'
import { OrtherComponent } from './orther/orther.component'
import { SettingComponent } from './setting.component'
import { WebsiteComponent } from './website/website.component'

/**
 * PHAN ANH VAN
 * edit: 18/3/2021
 * note: add brand, origin
 */

export const routes: Routes = [
    {
        path: '',
        component: SettingComponent,
        children: [
            { path: '', redirectTo: 'website' },
            { path: 'slide', loadChildren: () => import('./slide/slide.module').then(m => m.SlideModule) },
            { path: 'email', component: EmailComponent },
            { path: 'website', component: WebsiteComponent },
            // { path: 'orther', component: OrtherComponent },
            { path: 'menu', loadChildren: () => import('./menu/menu.module').then(m => m.SettingsMenuModule) },
            { path: 'product', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            // { path: 'brand', loadChildren: () => import('./brand/brand.module').then(m => m.SettingBrandModule) },
            // { path: 'origin', loadChildren: () => import('./origin/origin.module').then(m => m.OriginModule) },
            { path: 'content', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'library', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'link', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'customer', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'company', component: OrtherComponent },
            { path: 'social-network', component: OrtherComponent }

            // { path: 'attribute', loadChildren: () => import('./attribute/attribute.module').then(m => m.SettingAttributeModule) }
        ]
    }
]
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        AlertModule.forRoot(),
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot()
    ],
    providers: [EmailComponent, WebsiteComponent, OrtherComponent, TagsService],
    declarations: [SettingComponent, EmailComponent, WebsiteComponent, OrtherComponent]
})
export class SettingModule {}
