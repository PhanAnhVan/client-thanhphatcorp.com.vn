import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { AlertModule } from 'ngx-bootstrap/alert';
import { MenuGetListComponent } from './get-list/get-list.component';
import { MenuProcessComponent } from './process/process.component';

export const routes: Routes = [
    { path: '', redirectTo: 'get-list' },
    { path: 'get-list', component: MenuGetListComponent },
    { path: 'insert', component: MenuProcessComponent },
    { path: 'update/:id', component: MenuProcessComponent },
];
@NgModule({
    declarations: [
        MenuGetListComponent,
        MenuProcessComponent,],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        AlertModule.forRoot()
    ]
})
export class SettingsMenuModule { }
