import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Subscription } from 'rxjs'
import { Globals } from '../../../globals'
import { LinkService } from '../../../services/integrated/link.service'
import { TagsService } from '../../../services/integrated/tags.service'
import { uploadFileService } from '../../../services/integrated/upload.service'

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css']
})
export class ProcessProductComponent implements OnInit, OnDestroy {
    private connect: Subscription

    public id: number = 0
    public fm: FormGroup
    public price: FormGroup
    public seo: FormGroup

    private token: any = {
        getrow: 'get/product/getrow',
        getlistproductgroup: 'get/pages/grouptype',
        // getListOrigin: 'get/origin/getlist',
        // getListBrand: 'get/brand/getlist',
        process: 'set/product/process',
        updateSeo: 'set/product/updateSeo',
        updatePrice: 'set/product/updatePrice',

        getlistAttribute: 'get/attribute/getlist'
    }

    public data = { origin: [], brand: [], group: [], type: [] }
    public skip = false
    public images = new uploadFileService()
    public listimages = new uploadFileService()

    constructor(
        private routerAct: ActivatedRoute,
        public router: Router,
        public globals: Globals,
        private link: LinkService,
        private fb: FormBuilder,
        public tags: TagsService,
        private toastr: ToastrService
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id']

            if (this.id && this.id > 0) {
                this.get()
            } else {
                this.fmConfigs()
                this.priceConfigs()
                this.seoConfigs()
                this.productImages.set()
            }
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getrow':
                    let data = res.data
                    this.fmConfigs(data)
                    this.priceConfigs(data)
                    this.seoConfigs(data)
                    this.productImages.set(data)
                    if (res.data.attribute && res.data.attribute.length > 0) {
                        this.attribute._ini(res.data.attribute)
                    } else {
                        this.globals.send({ path: this.token.getlistAttribute, token: 'getlistAttribute' })
                    }
                    break

                // case 'getListBrand':
                //     if (res.status === 1) {
                //         this.data.brand = res.data
                //     }
                //     break

                // case 'getListOrigin':
                //     this.data.origin = res.data
                //     break

                case 'getlistproductgroup':
                    this.data.group = res.data
                    break

                case 'processProduct':
                    if (res.data > 0) {
                        this.id = res.data

                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/products/update/' + this.id])

                            window.scroll(0, 0)
                        }, 100)
                    }

                case 'updateSeo':
                case 'updatePrice':
                case 'updateImages':
                case 'updateProduct':
                case 'updateAttribute':
                    let type = res.status == 1 ? 'success' : res.status == 0 ? 'warning' : 'danger'
                    this.toastr[type](res.message, type, { timeOut: 1000 })

                    if (res.status == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/products/update/' + this.id])
                        }, 1000)
                    }
                    break

                case 'getlistAttribute':
                    this.attribute._ini(res.data)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        // this.globals.send({ path: this.token.getListBrand, token: 'getListBrand' })
        // this.globals.send({ path: this.token.getListOrigin, token: 'getListOrigin' })
        this.globals.send({ path: this.token.getlistproductgroup, token: 'getlistproductgroup', params: { type: 3 } })
    }

    get() {
        this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1, page_id: 0, origin_id: 0, brand_id: 0 }

        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],
            link: [item.link ? item.link : '', [Validators.required]],
            // code: [item.code ? item.code : '', [Validators.required]],
            page_id: [item.page_id ? item.page_id : 0, [Validators.required]],
            // brand_id: [item.brand_id ? item.brand_id : 0, [Validators.required]],
            // origin_id: [item.origin_id ? item.origin_id : 0, [Validators.required]],
            info: item.info ? item.info : '',
            hot: +item.hot ? +item.hot : 0,
            detail: item.detail ? item.detail : '',
            status: item.status && +item.status == 1 ? true : false
        })
    }

    onSubmit() {
        if (this.fm.valid) {
            const obj = this.fm.value

            obj.status = obj.status === true ? 1 : 0

            this.globals.send({ path: this.token.process, token: 'processProduct', data: obj, params: { id: this.id || 0 } })
        }
    }

    priceConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { inventory: 'Còn hàng' }
        this.price = this.fb.group({
            price_sale: [item.price_sale ? +item.price_sale : 0],
            inventory: item.inventory ? item.inventory : '',
            price: [item.price ? +item.price : 0],
            vat: item.vat ? +item.vat : 0
        })
    }

    updatePrice = () => {
        const obj = this.price.value

        obj.price = this.globals.price.format(obj.price)
        obj.price_sale = this.globals.price.format(obj.price_sale)

        this.globals.send({ path: this.token.updatePrice, token: 'updatePrice', data: obj, params: { id: this.id || 0 } })
    }

    seoConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1 }

        this.seo = this.fb.group({
            description: item.description ? item.description : ''
        })

        this.tags._set(item.keywords ? JSON.parse(item['keywords']) : '')
    }

    updateSeo = () => {
        const obj = this.seo.value

        obj.keywords = this.tags._get()

        this.globals.send({ path: this.token.updateSeo, token: 'updateSeo', data: obj, params: { id: this.id || 0 } })
    }

    public productImages = {
        tokenProcess: 'set/product/updateImages',

        set: (item: any = '') => {
            item = typeof item === 'object' ? item : { images: '', listimages: '' }
            const imagesConfig = {
                path: this.globals.BASE_API_URL + 'public/products/',
                data: item.images ? item.images : ''
            }
            this.images._ini(imagesConfig)
            const listimagesConfig = {
                path: this.globals.BASE_API_URL + 'public/products/',
                data: item.listimages ? item.listimages : '',
                multiple: true
            }
            this.listimages._ini(listimagesConfig)
        },

        process: () => {
            if (this.id > 0) {
                let data: any = {}
                data.images = this.images._get(true)
                data.listimages = this.listimages._get(true)

                this.globals.send({
                    path: this.productImages.tokenProcess,
                    token: 'updateImages',
                    data: data,
                    params: { id: this.id > 0 ? this.id : 0 }
                })
            }
        }
    }

    onChangeLink(e: { target: { value: any } }) {
        const url = this.link._convent(e.target.value)
        this.fm.value.link = url
    }

    attribute = {
        data: [],
        token: 'set/product/updateAttribute',
        _ini: data => {
            for (let i = 0; i < data.length; i++) {
                data[i].product_id = this.id || 0
                data[i].attribute_id = data[i].attribute_id ? data[i].attribute_id : data[i].id
                data[i].list = data[i].value ? JSON.parse(data[i].value) : []
                data[i].value = data[i].value ? data[i].value : ''
                delete data[i].id
            }
            this.attribute.data = data
        },
        keyup: (e, item) => {
            if (e.target.value && e.target.value.length > 0 && (e.which === 13 || e.which == 13)) {
                let is = this.attribute.push(e.target.value, item)
                if (is == true) {
                    e.target.value = ''
                }
                e.preventDefault()
                e.stopPropagation()
            }
        },
        push: (value, item) => {
            if (item.list.length > 0) {
                let skip = true
                for (let i = 0; i < item.list.length; i++) {
                    if (item.list[i].toLowerCase() == value.toLowerCase()) {
                        skip = false
                        break
                    }
                }
                if (skip == true) {
                    item.list.push(value)
                }
                return skip
            } else {
                item.list.push(value)
                return true
            }
        },
        remove(item, value) {
            let skip = false
            for (let i = 0; i < item.list.length; i++) {
                if (item.list[i].toLowerCase() == value.toLowerCase()) {
                    item.list.splice(i, 1)
                    skip = true
                    break
                }
            }
            return skip
        },
        onSubmit: () => {
            let data = this.attribute.getDataAttribute()
            this.globals.send({ path: this.attribute.token, token: 'updateAttribute', data: data, params: { id: this.id || 0 } })
        },
        getDataAttribute: () => {
            for (let i = 0; i < this.attribute.data.length; i++) {
                this.attribute.data[i].value = JSON.stringify(this.attribute.data[i].list)
            }
            return this.attribute.data
        }
    }
}
