import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { Globals } from './globals';
import { LoginComponent } from './login/login.component';
import { CarouselModule } from 'ngx-owl-carousel-o';

import { UserAuthGuard } from './services/auth/auth.guard';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const appRoutes = [
    { path: 'login', component: LoginComponent },
    {
        path: 'admin',
        loadChildren: () => import('./backend/backend.module').then(m => m.BackendModule), canActivate: [UserAuthGuard]
    },
    {
        path: '',
        loadChildren: () => import('./frontend/frontend.module').then(m => m.FrontendModule)
    },
]
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        CarouselModule
    ],
    providers: [
        Globals,
        // CookieService
        UserAuthGuard
    ],
    bootstrap: [
        AppComponent
    ],
})
export class AppModule { }
